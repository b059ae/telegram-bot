<?php

namespace app\commands\test;


use app\helpers\Telegram;
use app\models\TelegramChats;
use app\models\TelegramParams;
use SimpleXMLElement;
use TelegramBot\Api\Types\Chat;
use TelegramBot\Api\Types\ReplyKeyboardMarkup;
use yii\console\Controller;
use yii\helpers\VarDumper;

class TestController extends Controller
{

    public function actionIndex()
    {
        $lastUpdateId = TelegramParams::get('last_update_id');
        for ($i=0;$i<50;$i++){
            foreach (\Yii::$app->telegram->getUpdates($lastUpdateId + 1) as $item) {
                if ($lastUpdateId < $item->getUpdateId()) {
                    $lastUpdateId = $item->getUpdateId();
                }

                Telegram::doJob($item);

            }
            sleep(1);
            echo $i;
        }
        TelegramParams::set('last_update_id', $lastUpdateId);
    }






    //protected function


    /*protected function loadChat(Chat $tChat){

    }*/


}