<?php

namespace app\commands;

class WatcherDaemonController extends \vyants\daemon\controllers\WatcherDaemonController
{
    public $maxChildProcesses = 20;
    private $memoryLimit = 536870912;
    public $daemonsList = [];

    protected function getDaemonsList(){
        return $this->daemonsList;
    }
}