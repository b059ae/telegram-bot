<?php

namespace app\commands\daemons;

use app\commands\DaemonController;
use Yii;

class TelegramDaemonController extends DaemonController
{
    protected $sleep = 1;
    protected $lastUpdate;
    /**
     * @return array
     */
    protected function defineJobs()
    {
        sleep($this->sleep);

        $this->lastUpdateId = TelegramParams::get('last_update_id');

        foreach (\Yii::$app->telegram->getUpdates($lastUpdateId + 1) as $item) {

            $chat = new \app\models\Chat(['tChat' => $item->getMessage()->getChat()]);
            $messages = $chat->handleMessage($item->getMessage()->getText(), $item->getMessage()->getDate());

            if ($lastUpdateId < $item->getUpdateId()) {
                $lastUpdateId = $item->getUpdateId();
            }

            foreach ($messages as $message) {
                switch ($message['type']) {
                    case 'text':
                        \Yii::$app->telegram->sendMessage(
                            $chat->tChat->getId(),
                            $message['message'],
                            'Markdown',
                            false,
                            null,
                            $message['keyboard']
                        );
                        break;
                    case 'photo':
                        \Yii::$app->telegram->sendPhoto(
                            $chat->tChat->getId(),
                            $message['message']/*,
                            /*null,
                            null,
                            (new ReplyKeyboardMarkup([
                                ['opa pa','asdsadas'],
                            ]))*/
                        );
                        break;
                    /*case 'video':
                        \Yii::$app->telegram->sendMessage(
                            $chat->tChat->getId(),
                            $message['message']
                        );
                        break;*/
                }

            }
        }
        TelegramParams::set('last_update_id', $lastUpdateId);

        return Account::find()
            ->with('accountPersonalDaemon')
            ->where(['status' => Account::STATUS_ACTIVE])
            ->andWhere(['send_ocm' => Account::WAIT_SEND_OCM])
            ->andWhere(['reason_id' => [Account::REASON_CHECK, Account::REASON_REFUSED]])
            ->andWhere(['<', 'account.created_at', (time() - static::REQUEST_DELAY)])
            ->limit(10)
            ->all();
    }

    /**
     * @param Account $job
     * @return bool
     * @throws \Exception
     */
    protected function doJob($job)
    {
        $this->setContext($job);
        
        try {
            if (AccountHelper::sendToOcm($job)){
                //Запись лога
                Yii::info('Аккаунт с id ' . $job->id . ' отправлен', 'ocm');
            }else{
                Yii::warning('Аккаунт с id ' . $job->id . ' не отправлен, т.к. нет связи accountPersonal', 'ocm');
            }

        } catch (\Exception $e) {
            ///Запись лога
            Yii::error($e->getMessage(), 'ocm');
            $job->send_ocm = Account::ERROR_SEND_OCM;
            $job->save();
            return false;
        }
    }
}