<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 15.02.2017
 * Time: 20:47
 */

namespace app\models;


use yii\base\Object;

class Message extends Object
{

    /** @var  string */
    public $message;
    /** @var  TelegramChats */
    public $chat;
    /** @var  string */
    public $command;
    /** @var  string */
    public $argv;
    /** @var  int */
    public $date;

    /**
     * Разбирает составное сообщение на команду и аргументы
     */
    public function init(){

        if (mb_strlen($this->message, 'utf-8')>1 && mb_substr($this->message, 0, 1, 'utf-8') == '/'){
            //Отправлена команда
            if (preg_match('/^(\/[a-zA-Z0-9]+) ([\s\S]+)$/si',$this->message, $matches)){
                $this->command=$matches[1];
                $this->argv=$matches[2];
            }else{
                $this->command=$this->message;
            }
        }else{
            //Отправлено сообщение
            $this->argv=$this->message;
            $this->setCommand();
        }
        //Сохраняем сообщение
        $this->save();
    }


    protected function setCommand(){
        switch ($this->argv){
            case "\xF0\x9F\x93\xB0 Новости":
                $this->command = '/news';
                break;
            case "\xF0\x9F\x93\xBA ТВ":
                $this->command = '/tv';
                break;
            case "\xF0\x9F\x93\xB7 Последние фото":
                $this->command = '/photo';
                break;
            default:
                $this->command=$this->chat->last_command;
                break;
        }
    }

    /**
     * Сохранение сообщения
     * @throws \Exception
     */
    public function save(){
        $model = new TelegramMessages([
            'chat_id'=>$this->chat->chat_id,
            'message'=>$this->message,
            'created_at'=>$this->date,
        ]);
        if (!$model->save()){
            throw new \Exception(print_r($model->errors, true));
        }
    }
}