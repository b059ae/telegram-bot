<?php

namespace app\models;

use app\behaviors\CacheFlush;
use app\helpers\Data;
use Yii;

/**
 * This is the model class for table "telegram_params".
 *
 * @property string $name
 * @property string $value
 */
class TelegramParams extends \yii\db\ActiveRecord
{
    const CACHE_KEY = 'telegram_params';

    static $_data;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telegram_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Key',
            'value' => 'Value',
        ];
    }

    public function behaviors()
    {
        return [
            CacheFlush::className()
        ];
    }

    public static function get($name)
    {
        if(!self::$_data){
            self::$_data =  Data::cache(self::CACHE_KEY, 3600, function(){
                $result = [];
                try {
                    foreach (static::find()->all() as $setting) {
                        $result[$setting->name] = $setting->value;
                    }
                }catch(\yii\db\Exception $e){}
                return $result;
            });
        }
        return isset(self::$_data[$name]) ? self::$_data[$name] : null;
    }

    public static function set($name, $value)
    {
        $value = (string) $value;
        if(self::get($name)){
            $setting = static::find()->where(['name' => $name])->one();
            $setting->value = $value;
        } else {
            $setting = new static([
                'name' => $name,
                'value' => $value,
            ]);
        }
        if (!$setting->save()){
            throw new \Exception(print_r($setting->errors, true));
        }
    }

}
