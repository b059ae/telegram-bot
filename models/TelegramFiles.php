<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telegram_files".
 *
 * @property int $id
 * @property int $chat_id
 * @property string $file_id
 * @property int $created_at
 * @property int $updated_at
 */
class TelegramFiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telegram_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_id', 'created_at', 'updated_at'], 'integer'],
            [['file_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => 'Chat ID',
            'file_id' => 'File ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
