<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 15.02.2017
 * Time: 20:47
 */

namespace app\models;


use app\helpers\Data;
use SimpleXMLElement;
use TelegramBot\Api\Types\ReplyKeyboardMarkup;
use yii\base\Object;

class Chat extends Object
{

    /** @var TelegramChats */
    protected $chat;
    /** @var  \TelegramBot\Api\Types\Chat */
    public $tChat;
    /** @var  array */
    protected $return = [];

    /**
     * Загрузка чата с пользователем
     * @param Chat $tChat
     */
    public function init()
    {
        $this->chat = TelegramChats::findOne(['chat_id' => $this->tChat->getId()]);
        if (!$this->chat) {
            $this->chat = new TelegramChats(['chat_id' => $this->tChat->getId()]);
        }

        $this->chat->setAttributes([
            'name' => static::getFullName($this->tChat->getFirstName(), $this->tChat->getLastName()),
            'username' => $this->tChat->getUsername(),
        ]);
    }


    /**
     * Обработка сооб`щения пользователя
     * @param string $text
     * @param string $date
     */
    public function handleMessage($text, $date)
    {
        $message = new Message([
            'message' => $text,
            'chat' => $this->chat,
            'date' => $date,
        ]);

        switch ($message->command) {
            case '/start':
                $this->handleStart($message);
                break;
            case '/help':
                $this->handleHelp();
                break;
            case '/news':
                $this->handleNews();
                break;
            case '/tv':
                $this->handleTv();
                break;
            /*case '/hello':
                $this->handleHello();
                break;*/
            case '/photo':
                $this->handlePhoto();
                break;
            default:
                $this->handleHelp();
                break;
        }
        if (!$this->chat->save()) {
            throw new \Exception(print_r($this->chat->errors, true));
        }
        return $this->return;
    }

    /**
     * Отрисовывает клавиатуру
     * Emoji http://apps.timwhitlock.info/emoji/tables/unicode
     * @return ReplyKeyboardMarkup
     */
    protected function getKeyboard()
    {
        return new ReplyKeyboardMarkup([
            ["\xF0\x9F\x93\xB0 Новости", "\xF0\x9F\x93\xBA ТВ"],
            ["\xF0\x9F\x93\xB7 Последние фото"],
        ],
            null,
            true);
    }

    /**
     * Проверяет авторизован ли пользователь
     * @param bool $needMessage Необходимо отправить пользователю сообщение, что он не авторизован
     * @return bool
     */
    protected function isAuth($needMessage = false)
    {
        if (!$this->chat->is_auth) {
            //Необходимо отправить пользователю сообщение, что он не авторизован
            if ($needMessage) {
                $this->setLastCommand('/start');
                $this->return[] = [
                    'type' => 'text',
                    'message' => 'Необходимо авторизоваться, пароль ' . \Yii::$app->params['telegramBotPassword'],
                ];
            }
            return false;
        }

        return true;
    }

    /**
     * Отправляет фото
     */
    protected function handlePhoto()
    {
        if (!$this->isAuth(true)) {
            return;
        }
        
        $xmlstr = Data::cache('flickr_public', 3600, function () {
            $xmlstr = null;
            try {
                $xmlstr = file_get_contents('https://api.flickr.com/services/feeds/photos_public.gne');
            } catch (\yii\db\Exception $e) {
            }
            return $xmlstr;
        });

        if (!$xmlstr) {
            throw new \Exception('Error connect to RSS: ');
        };
        $xml = new SimpleXMLElement($xmlstr);
        if (!$xml) {
            throw new \Exception('Error parse RSS: ');
        };

        $json = json_encode($xml);
        $xml = json_decode($json, true);

        $items = $xml['entry'];

        shuffle($items);
        foreach (array_slice($items, 0, 3) as $item) {
            foreach($item['link'] as $link){
                if ($link['@attributes']['rel'] == 'enclosure'){
                    $this->return[] = [
                        'type' => 'photo',
                        'message' => $link['@attributes']['href'],
                    ];
                }
            }
        }
    }

    /**
     * Отправляет Последние три новости
     */
    protected function handleNews()
    {
        if (!$this->isAuth(true)) {
            return;
        }

        $xmlstr = Data::cache('lenta_news', 3600, function () {
            $xmlstr = null;
            try {
                $xmlstr = file_get_contents('https://lenta.ru/rss/top7');
            } catch (\yii\db\Exception $e) {
            }
            return $xmlstr;
        });

        if (!$xmlstr) {
            throw new \Exception('Error connect to RSS: ');
        };
        $xml = new SimpleXMLElement($xmlstr);
        if (!$xml) {
            throw new \Exception('Error parse RSS: ');
        };

        $items = $xml->xpath('//item');
        shuffle($items);
        foreach (array_slice($items, 0, 3) as $item) {
            $this->return[] = [
                'type' => 'photo',
                'message' => $item->enclosure['url'],
            ];
            $this->return[] = [
                'type' => 'text',
                'keyboard' => null,
                'message' => $item->description,
            ];
        }
    }

    /**
     * Отправляет Видео с ютуба
     */
    protected function handleTv()
    {
        if (!$this->isAuth(true)) {
            return;
        }

        $xmlstr = Data::cache('youtube_feed', 3600, function () {
            $xmlstr = null;
            try {
                $xmlstr = file_get_contents('https://www.youtube.com/feeds/videos.xml?channel_id=UC4aP1tC3Z4o2Yt9CXMo-4WQ');
            } catch (\yii\db\Exception $e) {
            }
            return $xmlstr;
        });

        if (!$xmlstr) {
            throw new \Exception('Error connect to RSS: ');
        };
        $xml = new SimpleXMLElement($xmlstr);
        if (!$xml) {
            throw new \Exception('Error parse RSS: ');
        };

        $json = json_encode($xml);
        $xml = json_decode($json, true);

        $items = $xml['entry'];

        shuffle($items);
        foreach (array_slice($items, 0, 1) as $item) {

            $this->return[] = [
                'type' => 'text',
                'keyboard' => null,
                'message' => $item['link']['@attributes']['href'],
            ];
        }
    }

    /**
     * Обработка авторизации
     * @param Message $message
     * @return string
     */
    protected function handleStart(Message $message)
    {
        if (!$this->isAuth(false)) {
            if (!$message->argv) {
                $this->setLastCommand($message->command);
                $this->return[] = [
                    'type' => 'text',
                    'keyboard' => null,
                    'message' => 'Пожалуйста введите пароль для продолжения.',
                ];
                return;
            }

            if ($message->argv != \Yii::$app->params['telegramBotPassword']) {
                $this->setLastCommand($message->command);
                $this->return[] = [
                    'type' => 'text',
                    'keyboard' => null,
                    'message' => "Неверный пароль",
                ];
                return;
            }
        }

        $this->chat->is_auth = 1;
        $this->return[] = [
            'type' => 'text',
            'keyboard' => null,
            'message' => "Вы успешно авторизованы",
        ];
        $this->handleHelp();

        return;
    }

    /**
     * Вызов справки
     * @return string
     */
    protected function handleHelp()
    {
        if (!$this->isAuth(true)) {
            return;
        }

        $this->setLastCommand(null);
        $this->return[] = [
            'type' => 'text',
            'keyboard' => $this->getKeyboard(),
            'message' => "С помощью меня вы можете:\n
_Получать 3 свежие новости c ленты.ру._\n
_Смотреть ТВ-трансляциии на ютубе_\n
_Получать новые 3 фото с фликера_\n
Для продолжения нажмите кнопку меню",
        ];
        return;
    }

    /**
     * Вызов справки
     * @return string
     */
    protected function handleHello()
    {
        if (!$this->isAuth(true)) {
            return;
        }
        $this->setLastCommand(null);
        $this->return[] = [
            'type' => 'text',
            'message' => 'Привет, *это* _закрытая_ функция',
        ];
        return;
    }

    /**
     * Устанавливает последнюю, команду, которую отправил пользователь
     * @param string $command
     */
    protected function setLastCommand($command)
    {
        $this->chat->last_command = $command;
    }

    /**
     * Возвращает полное имя
     * @param string $firstName
     * @param string $lastName
     * @return string
     */
    public static function getFullName($firstName, $lastName)
    {
        return $firstName . ' ' . $lastName;
    }
}