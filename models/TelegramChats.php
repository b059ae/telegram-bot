<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telegram_chats".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property int $chat_id
 * @property string $last_command
 * @property int $is_auth
 * @property int $created_at
 * @property int $updated_at
 */
class TelegramChats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telegram_chats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_id', 'created_at', 'updated_at', 'is_auth'], 'integer'],
            [['name', 'username', 'last_command'], 'string', 'max' => 255],
        ];
    }
}
