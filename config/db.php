<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'sqlite:'.realpath(__DIR__."/../runtime")."/data.db",
];
