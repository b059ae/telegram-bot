<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'controllerMap' => [
        'watcher-daemon' => [
            'class' => 'app\commands\WatcherDaemonController',
            'daemonFolder'=>'daemons',
            'daemonsList' => [
                //При редактировании списка демонов НЕ ЗАБЫВАТЬ ИЗМЕНИТЬ daemon.sh

                //Обработка сообщений телеграмма
                'Telegram' => ['className' => 'TelegramDaemonController', 'enabled' => true],
            ],
        ],
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'telegram' => [
            'class' => 'SonkoDmitry\Yii\TelegramBot\Component',
            'apiToken' => '371808733:AAEFWjRXf5EBVcz0jm1V9fivSMAx5pgBmwA',
        ],
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
