<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16.02.17
 * Time: 12:26
 */

namespace app\helpers;


use app\models\TelegramParams;
use TelegramBot\Api\Types\Update;

class Telegram
{
    /**
     * Получает запрос от пользователя, обрабатывает и отправляет ответ
     * @param $item
     * @return mixed
     * @internal Update $item
     */
    public static function doJob($item)
    {

            $chat = new \app\models\Chat(['tChat' => $item->getMessage()->getChat()]);
            $messages = $chat->handleMessage($item->getMessage()->getText(), $item->getMessage()->getDate());

            foreach ($messages as $message) {
                switch ($message['type']) {
                    case 'text':
                        \Yii::$app->telegram->sendMessage(
                            $chat->tChat->getId(),
                            $message['message'],
                            'Markdown',
                            false,
                            null,
                            $message['keyboard']
                        );
                        break;
                    case 'photo':
                        \Yii::$app->telegram->sendPhoto(
                            $chat->tChat->getId(),
                            $message['message']/*,
                            /*null,
                            null,
                            (new ReplyKeyboardMarkup([
                                ['opa pa','asdsadas'],
                            ]))*/
                        );
                        break;
                    /*case 'video':
                        \Yii::$app->telegram->sendMessage(
                            $chat->tChat->getId(),
                            $message['message']
                        );
                        break;*/
                }

            }
    }
}