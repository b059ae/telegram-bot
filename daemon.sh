#!/bin/sh
### BEGIN INIT INFO
# Provides: watcher-daemon
# Default-Start: 3 5
# Default-Stop: 0 1 6
# Required-Start: $remote_fs $syslog
# Required-Stop: $remote_fs $syslog
# Short-Description: Run watcher-daemon
# Description: Run watcher-daemon
### END INIT INFO
PATH_YII=./yii
DAEMON=watcher-daemon
CONFIG=--demonize=1
SUFFIX=DaemonController
TASKS="Watcher"

#test -x PATH_YII || exit 0
case "$1" in
start)
$PATH_YII $DAEMON $CONFIG
;;
stop)
for controller in $TASKS
do
 pkill -f $controller$SUFFIX
done
;;
restart)
for controller in $TASKS
do
 pkill -f $controller$SUFFIX
done
sleep 10
$PATH_YII $DAEMON $CONFIG
;;
*)
echo "Usage: $0 {start|stop|restart}"
exit 1
;;
esac
exit 0