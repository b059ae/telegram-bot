<?php

use yii\db\Migration;

class m170215_092244_init extends Migration
{
    /**
     * @var string Опции создания таблицы
     */
    protected $tableOption = null;

    /**
     * Настройка названий таблиц и дополнительных опций движка БД
     */
    public function init()
    {
        // Инициализация табличных опций при условии создания таблиц в среде MYSQL
        if($this->db->driverName == 'mysql') {
            $this->tableOption = 'ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci';
        }
        // Создание имен таблиц
    }

    public function up()
    {
        $this->createTable('telegram_chats', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'username' => $this->string(),
            'chat_id' => $this->integer(),
            'last_command' => $this->string(),
            'is_auth' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOption);

        $this->createIndex('ix_tc_chat_id', 'telegram_chats', 'chat_id');

        $this->createTable('telegram_messages', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(),
            'message' => $this->string(),
            'created_at' => $this->integer(),
        ], $this->tableOption);

        $this->createIndex('ix_tm_chat_id', 'telegram_messages', 'chat_id');

        $this->createTable('telegram_files', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(),
            'file_id' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOption);

        $this->createIndex('ix_tf_chat_id', 'telegram_files', 'chat_id');

        $this->createTable('telegram_params', [
            'name' => 'TEXT PRIMARY KEY NOT NULL',
            'value' => $this->string(),
        ], $this->tableOption);

        $this->insert('telegram_params',[
            'name'=>'last_update_id',
            'value' => 1,
        ]);

    }

    public function down()
    {
        $this->dropTable('telegram_chats');
        $this->dropTable('telegram_messages');
        $this->dropTable('telegram_files');
        $this->dropTable('telegram_params');
    }

}
